using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPosition : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RePositionWithDelay());
    }

    IEnumerator RePositionWithDelay(){
        while(true){
            SetRandomPosition();
            yield return new WaitForSeconds(5f);
        }
    }

    void SetRandomPosition(){
        float x = Random.Range(-5f, 5f);
        float z = Random.Range(-5f, 5f);

        transform.position = new Vector3(x, 0f, z);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LookMoveTo : MonoBehaviour
{
    public GameObject ground;
    private Transform camera;
    public Transform infoBubble;
    public Text infoText;

    private void Start() {
        camera = Camera.main.transform;
        if(infoBubble != null)
        {
            infoText = GetComponentInChildren<Text>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray;
        RaycastHit[] hits;
        GameObject hitObject;

        Debug.DrawRay(camera.position, camera.rotation * Vector3.forward * 100f);

        ray = new Ray(camera.position, camera.rotation * Vector3.forward);
        hits = Physics.RaycastAll(ray);

        for (int i = 0; i < hits.Length;i++){
            RaycastHit hit = hits[i];
            hitObject = hit.collider.gameObject;
            if(hitObject == ground){
                if(infoBubble != null)
                {
                    infoText.text = "X: " + hit.point.x.ToString("F2") +
                                    ", " + hit.point.y.ToString("F2");
                    infoBubble.LookAt(camera.position);
                    infoBubble.Rotate(0, 180f, 0);
                }
                transform.position = hit.point;
            }
        }
    }
}

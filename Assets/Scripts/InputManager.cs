using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    private XRNode xrNode = XRNode.LeftHand;
    private List<InputDevice> devices = new List<InputDevice>();
    private InputDevice device;

    void GetDevice(){
        InputDevices.GetDevicesAtXRNode(xrNode, devices);
        device = devices.LastOrDefault();
    }

    private void OnEnable() {
        if(!device.isValid){
            GetDevice();
        }

        Debug.Log("device : " + device.name + " role: " + device.role.ToString());
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!device.isValid){
            GetDevice();
        }
        
        if(device.TryGetFeatureValue(CommonUsages.triggerButton, out bool triggerButtonAction) && triggerButtonAction){
            Debug.Log("Trigger button pressed");
        }
    }
}

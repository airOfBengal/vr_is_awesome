using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poppable : MonoBehaviour
{
    public GameObject popEffectPrefab;

    private void OnCollisionEnter(Collision other) {
        if(transform.parent == null &&
        other.gameObject.GetComponent<Poppable>() == null){
            PopBalloon();
        }
    }

    private void PopBalloon()
    {
        if(popEffectPrefab != null)
        {
            GameObject effect = Instantiate(popEffectPrefab,
            transform.position, transform.rotation);
            Destroy(effect, 1f);
        }
        Destroy(gameObject);
    }
}

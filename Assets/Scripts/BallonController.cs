using UnityEngine;

public class BallonController : MonoBehaviour
{
    public GameObject balloonPrefab;
    public float forceStrength = 20f;
    public float growthRate = 1.5f;
    private GameObject balloon;

    private Rigidbody rb;


    // Update is called once per frame
    void Update()
    {
        if(balloon != null)
        {
            GrowBalloon();
        }
    }

    private void GrowBalloon()
    {
        float growThisFrame = growthRate * Time.deltaTime;
        Vector3 changeScale = balloon.transform.localScale * growThisFrame;
        balloon.transform.localScale += changeScale;
    }

    public void ReleaseBalloon()
    {
        rb.isKinematic = false;
        if(balloon != null)
            balloon.transform.parent = null;
        //Rigidbody rb = balloon.GetComponent<Rigidbody>();
        rb.AddForce(Vector3.up * forceStrength);

        Destroy(balloon, 10f);
        balloon = null;
    }

    public void CreateBalloon(GameObject parent)
    {
        balloon = Instantiate(balloonPrefab, parent.transform);
        balloon.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        rb = balloon.GetComponent<Rigidbody>();
        rb.isKinematic = true;
    }
}
